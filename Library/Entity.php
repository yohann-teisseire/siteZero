<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 22/07/15
 * Time: 11:54
 */

namespace Library;


abstract class Entity implements \ArrayAccess
{

    protected $erreurs = [];
    protected $id;

    public function __construct(array $donnees = array())
    {
        if (!empty($donnees)) {
            $this->hydrate($donnees);
        }
    }

    public function isNew()
    {
        return empty($this->id);
    }

    public function id()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
    }

    public function hydrate(array $donnees)
    {
        foreach ($donnees as $attr => $val) {
            $methode = 'set' . ucfirst($attr);

            if (is_callable(array($this, $methode))) {
                $this->$methode($val);
            }
        }
    }

    public function offsetGet($var)
    {
        if (isset($this->var) && is_callable(array($this, $var))) {
            return $this->$var();
        }
    }

    public function offsetSet($var, $val)
    {
        $methode = 'set' . ucfirst($var);
        if (isset($this->$var) && is_callable(array($this, $methode))) {
            $this->$methode($val);
        }
    }

    public function offsetExists($var)
    {
        return isset($this->$var) && is_callable(array($this, $var));
    }

    public function offsetUnset($var)
    {
        throw new \Exception('Impossible de supprimer cette valeur');
    }
}