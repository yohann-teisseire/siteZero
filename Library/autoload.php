<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 21/07/15
 * Time: 17:00
 */

function  autoload($class){
    require '../'. str_replace('\\', '/', $class). '.php';
}

spl_autoload_register('autoload');
