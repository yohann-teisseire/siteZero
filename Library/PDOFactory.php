<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 22/07/15
 * Time: 11:43
 */

namespace Library;


class PDOFactory {

    public static function getMysqlConnexion(){
        $db = new \PDO('mysql:host=localhost;dbname=news','root','root');
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $db;
    }

}