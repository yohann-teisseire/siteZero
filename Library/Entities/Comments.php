<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 23/07/15
 * Time: 14:56
 */

namespace Library\Entities;


use Library\Entity;

class Comments extends Entity
{

    protected $news;
    protected $auteur;
    protected $contenu;
    protected $date;

    const AUTEUR_INVALIDE = 1;
    const CONTENU_INVALIDE = 2;


    public function isValid()
    {
        return !(empty($this->auteur) || empty($this->contenu));
    }

    /**
     * @return mixed
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param mixed $news
     */
    public function setNews($news)
    {
        $this->news = (int)$news;
    }

    /**
     * @return mixed
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * @param mixed $auteur
     */
    public function setAuteur($auteur)
    {
        if (!is_string($auteur) || empty($auteur)) {
            $this->erreurs[] = self::AUTEUR_INVALIDE;
        } else {
            $this->auteur = $auteur;
        }

    }

    /**
     * @return mixed
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param mixed $contenu
     */
    public function setContenu($contenu)
    {
        if (!is_string($contenu) || empty($contenu)) {
            $this->erreurs[] = self::CONTENU_INVALIDE;
        } else {
            $this->contenu = $contenu;
        }
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getErreurs()
    {
        return $this->erreurs;
    }

    /**
     * @param array $erreurs
     */
    public function setErreurs($erreurs)
    {
        $this->erreurs = $erreurs;
    }
}