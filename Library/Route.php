<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 21/07/15
 * Time: 18:34
 */

namespace Library;


class Route
{

    protected $action;
    protected $module;
    protected $url;
    protected $varsNames;
    protected $vars = [];

    public function __construct($action, $module, $url, array $varsNames)
    {
        $this->setAction($action);
        $this->setModule($module);
        $this->setUrl($url);
        $this->setVarsNames($varsNames);
    }

    public function hasVars()
    {
        return !empty($this->varsNames);
    }

    public function match($url)
    {
        if (preg_match('‘^' . $this->url . '$‘', $url, $matches)) {
            return $matches;
        } else {
            return false;
        }
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        if (is_string($action)) {
            $this->action = $action;
        }
    }

    /**
     * @param mixed $module
     */
    public function setModule($module)
    {
        if (is_string($module)) {
            $this->module = $module;
        }
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        if (is_string($url)) {
            $this->url = $url;
        }
    }

    /**
     * @param mixed $varsNames
     */
    public function setVarsNames(array $varsNames)
    {
        $this->varsNames = $varsNames;
    }

    /**
     * @param array $vars
     */
    public function setVars(array $vars)
    {
        $this->vars = $vars;
    }

    /**
     * @return mixed
     */
    public function action()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function module()
    {
        return $this->module;
    }

    /**
     * @return mixed
     */
    public function url()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function varsNames()
    {
        return $this->varsNames;
    }

    /**
     * @return array
     */
    public function vars()
    {
        return $this->vars;
    }


}