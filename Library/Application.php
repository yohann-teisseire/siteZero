<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 21/07/15
 * Time: 17:25
 */

namespace Library;


abstract class Application
{

    protected $httpRequest;
    protected $httpResponse;
    protected $name;
    protected $user;

    public function __construct()
    {
        $this->httpRequest = new HTTPRequest($this);
        $this->httpResponse = new HTTPResponse($this);
        $this->name = '';
        $this->user = new User($this);
    }

    public function getController()
    {
        $router = new Router();

        $xml = new \DOMDocument();
        $xml->load(__DIR__ . '/../Applications/' . $this->name . '/Config/routes.xml');

        $routes = $xml->getElementsByTagName('route');

        foreach ($routes as $route) {
            if ($route->hasAttribute('vars')) {
                $vars = explode(',', $route->getAttribute('vars'));
            }

            $router->addRoute(new Route($route->getAttribute('url'), $route->getAttribute('module'), $route->getAttribute('action'), $vars));
        }

        try {
            $matchedRoute = $router->getRoute($this->httpRequest->requestURI());

        } catch (\RuntimeException $e) {

            if ($e->getCode() == Router::NO_ROUTE) {
                $this->httpResponse->redirect404();
            }
        }

        //on ajout eles variables get au talbeau $GET
        $_GET = array_merge($_GET, $matchedRoute->vars());

        //instanciation du controller
        $controllerClass = 'Applications\\' . $this->name . '\\Modules\\' . $matchedRoute->module() . '\\' . $matchedRoute->action() . 'Controller';

        return new $controllerClass($this, $matchedRoute->module(), $matchedRoute->action());
    }

    abstract public function run();

    /**
     * @return mixed
     */
    public function httpRequest()
    {
        return $this->httpRequest;
    }

    /**
     * @return mixed
     */
    public function httpResponse()
    {
        return $this->httpResponse;
    }

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->name;
    }
}