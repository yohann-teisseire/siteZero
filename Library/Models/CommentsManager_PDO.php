<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 23/07/15
 * Time: 14:55
 */

namespace Library\Models;


use Library\Entities\Comments;

class CommentsManager_PDO extends CommentsManager{

    protected function delete($id){
        $this->dao->exec('DELETE FROM comments WHERE id ='.(int)$id);
    }

    protected function modify(Comments $comments){
        $req = $this->dao->prepare('UPDATE comments SET auteur = :auteur, contenu = :contenu WHERE id = :id');

        $req->bindValue(':auteur', $comments->getAuteur());
        $req->bindValue(':contenu', $comments->getContenu());
        $req->bindValue(':id', $comments->id(), \PDO::PARAM_INT);

        $req->execute();
    }

    public function get($id){
        $req = $this->dao->prepare('SELECT id, news, auteur, contenu FROM comments WHERE id = :id');

        $req->bindValue(':id', (int) $id, \PDO::PARAM_INT);

        $req->execute();

        $req->setFetchmode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entity\Commment');

        return $req->fetch();
    }

    protected function add(Comments $comments){
        $sql = $this->dao->prepare('INSERT INTO comments SET news = :news, auteur = :auteur, contenu = :contenu, date = NOW()');

        $sql->bindValue(':news', $comments->getNews(), \PDO::PARAM_INT);
        $sql->bindValue(':auteur', $comments->getAuteur());
        $sql->bindValue(':contenu', $comments->getContenu());

        $sql->execute();

        $comments->setId($this->dao->lastInsertId());
    }

    public function getListOf($news){
        if(!ctype_digit($news)){
            throw new \InvalidArgumentException('Identifiant passé doit être un nombre entier valide');
        }

        $sql = $this->dao->prepare('SELECT * FROM comments WHERE news = :news');
        $sql->bindValue(':news', $news, \PDO::PARAM_INT);
        $sql->execute();

        $sql->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entities\Comments');

        $comments = $sql->fetchAll();

        foreach($comments as $comment){
            $comment->setDate(new \DateTime($comment->date()));
        }

        return $comments;
    }
}