<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 23/07/15
 * Time: 11:33
 */

namespace Library\Models;


use Library\Entities\News;
use Library\Manager;

abstract class NewsManager extends Manager
{

    abstract protected function delete($id);

    abstract protected function modify(News $news);

    abstract protected function add(News $news);

    public function save(News $news)
    {
        if ($news->isValid()) {
            $news->isNew() ? $this->add($news) : $this->modify($news);
        } else {
            throw new \RuntimeException('La news doit etre vallide pour etre enregistrée');
        }
    }

    abstract public function count();

    abstract public function getList($debut = -1, $limite = -1);

    abstract public function getUnique($id);
}