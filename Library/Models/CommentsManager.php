<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 23/07/15
 * Time: 14:55
 */

namespace Library\Models;

use Library\Entities\Comments;
use Library\Manager;

abstract class CommentsManager extends Manager
{

    abstract protected function delete($id);

    abstract protected function modify(Comments $comments);

    abstract protected function  get($id);

    abstract protected function add(Comments $comments);

    public function save(Comments $comments)
    {
        if ($comments->isValid()) {
            $comments->isNew() ? $this->add($comments) : $this->modify($comments);
        } else {
            throw new \RuntimeException('Le commentaire doit etre validé pour être enregistré');
        }
    }

    abstract public function getListOf($news);
}