<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 23/07/15
 * Time: 11:33
 */

namespace Library\Models;


use Library\Entities\News;

class NewsManager_PDO extends NewsManager {

    public function delete($id){
       $this->dao->exec('DELETE FROM news WHERE id = '.(int) $id);
    }

    protected function modify(News $news){
        $req = $this->dao->prepare('UPDATE news SET auteur = auteur, titre = :titre, contenu = :contenu, dateModif = NOW() WHERE id = :id');

        $req->bindValue(':auteur', $news->getAuteur());
        $req->bindValue(':titre', $news->getTitre());
        $req->bindValue(':contenu', $news->getContenu());
        $req->bindValue(':id', \PDO::PARAM_INT);

        $req->execute();
    }

    public function add(News $news){

        $req = $this->dao->prepare('INSERT INTO news SET auteur = :auteur, titre = :titre, contenu = :contenu, dateAjout = NOW(), dateModif = NOW()');
        $req->bindValue(':auteur', $news->getTitre());
        $req->bindValue(':titre', $news->getAuteur());
        $req->bindValue(':contenu', $news->getContenu());

        $req->execute();
    }

     public function count(){
         return $this->dao->query('SELECT COUNT(*) FROM news')->fetchColumn();
     }
     public function getList($debut = -1, $limite = -1){

         $sql = 'SELECT * FROM news ORDER BY id DESC';

         if($debut !=-1 || $limite!=-1){
             $sql .= 'LIMIT'.(int) $limite. 'OFFSET' .(int) $debut;
         }

         $req = $this->dao->query($sql);
         $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Library\Entities\News');

         $listeNews = $req->fetchAll();

         foreach($listeNews as $news){
             $news->setDateAjout(new \DateTime($news->dateAjout));
             $news->setDateModif(new \DateTime($news->dateModif));
         }

         $req->closeCursor();

         return $listeNews;
     }

     public function getUnique($id){

         $req = $this->dao->prepare('SELECT * FROM news WHERE id = :id');
         $req->bindValue(':id', (int)$id, \PDO::PARAM_INT);
         $req->execute();

         $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Library\Entities\News');

         if($news = $req->fetch()){
             $news->setDateAjout(new \DateTime($news->dateAjout));
             $news->setDateModif(new \DateTime($news->dateModif));

             return $news;
         }
         return null;
     }
}