<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 22/07/15
 * Time: 11:50
 */

namespace Library;


abstract class Manager
{
    protected $dao;

    public function __construct($dao)
    {
        $this->dao = $dao;
    }
}