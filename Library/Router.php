<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 21/07/15
 * Time: 17:50
 */

namespace Library;


class Router
{

    protected $routes = [];

    const NO_ROUTE = 1;

    public function addRoute(Route $route)
    {

        if (!in_array($route, $this->routes)) {
            $this->routes[] = $route;
        }
    }

    public function getRoute($url)
    {

        foreach ($this->routes as $route) {

            //si la route correspond à l'url
            if (($varsValue = $route->match($url)) !== false) {
                //si elle a des variables
                if ($route->hasVars()) {

                    $varsNames = $route->varsNames();
                    $listVars = [];

                    // oncréé un tableau clé/valeur
                    foreach ($varsValue as $key => $match) {

                        if ($key !== 0) {

                            $listVars[$varsNames[$key - 1]] = $match;
                        }
                    }

                    $route->setVars($listVars);
                }

                return $route;
            }
        }

        throw new \RuntimeException('Aucune route de correspond à URL donnée', self::NO_ROUTE);
    }

}