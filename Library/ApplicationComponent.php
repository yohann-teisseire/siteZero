<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 21/07/15
 * Time: 17:36
 */

namespace Library;


abstract class ApplicationComponent
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function app()
    {
        return $this->app;
    }
}