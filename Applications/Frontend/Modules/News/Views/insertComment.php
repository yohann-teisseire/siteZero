<h2>Ajouter un commentaire</h2>
<form action="" method="post">
    <p>
        <?php if (isset($erreurs) && in_array(\Library\Entities\Comments::AUTEUR_INVALIDE, $erreurs)) {
            echo '<div class="alert alert-danger">Auteur est invalide</div>';
        }
        ?>
        <label>Pseudo</label>
        <input type="text" name="pseudo"
               value="<?php if (isset($comment)) echo htmlspecialchars($comment['auteur']); ?>"/><br>

        <?php if (isset($erreurs) && in_array(\Library\Entities\Comments::CONTENU_INVALIDE, $erreurs)) {
            echo '<div class="alert alert-danger">Contenu est invalide</div>';
        }
        ?>
        <label>Contenu</label>
        <textarea name="contenu" rows="7" cols="50">
            <?php if(isset($comment)) echo htmlspecialchars($comment['contenu']); ?>
        </textarea><br>

        <input class="btn btn-primary" type="submit" value="commenter" />
    </p>
</form>