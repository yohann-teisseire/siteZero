<p>Par <em><?php echo $news['auteur']; ?></em>, le <?php echo $news['dateAjout']->format('d-m-Y à H:i'); ?></p>

<h2><?php echo $news['titre']; ?></h2>
<p><?php echo $news['contenu']; ?></p>

<?php
if ($news['dateAjout'] != $news['dateModif']) {
    echo '<p>Le texte a été modifié le ' . $news['dateModif']->format('d-m-Y à H:i') . '</p>';
}
?>
<p><a href="commenter-<?php echo $news['id']; ?>.php"></a>Ajouter un commentaire</p>

<?php
if (empty($comments)) {
    echo 'Aucun commentaires, Soyez le premier';
}
foreach ($comments as $comment) {
    ?>
    <fieldset>
        <legend>posté par : <?php echo htmlspecialchars($comment['auteur']); ?>,
            le <?php echo $comment['date']->format('d-m-Y à H:i'); ?></legend>
        <p><?php echo nl2br(htmlspecialchars($comment['contenu'])); ?></p>
        <?php
        if($user->isAuthenticated()){ ?>
            <p><a class="btn btn-warning" href="admin/comment-update-<?php echo $comment['id']; ?>.php">modifier un commentaire</a></p>
        <?php } ?>
        <?php if($user->isAuthenticated()){ ?>
        <p><a class="btn btn-danger" href="admin/comment-delete-<?php echo $comment['id']; ?>.php">Supprimer un commentaire</a></p>
        <?php } ?>
    </fieldset>
<?php
}
?>
<p><a class="btn btn-primary" href="commenter-<?php echo $news['id']; ?>.php">Ajouter un commentaire</a></p>
