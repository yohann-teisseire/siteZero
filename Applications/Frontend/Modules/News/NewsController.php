<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 23/07/15
 * Time: 11:32
 */

namespace Applications\Frontend\Modules;

use Library\BackController;
use Library\Entities\Comments;
use Library\HTTPRequest;


class NewsController extends BackController
{

    public function executeInsertComment(HTTPRequest $request)
    {
        $this->page->addVar('title', 'Ajout d\'un commentaire');

        if ($request->postExists('pseudo')) {
            $comment = new Comments(array(
                'news' => $request->getData('news'),
                'auteur' => $request->getData('pseudo'),
                'contenu' => $request->getData('contenu')
            ));

            if ($comment->isValid()) {
                $this->managers->getManagerOf('Comments')->save($comment);

                $this->app->user()->setFlash('Le commentaire a bien ete ajouté merci');

                $this->app->httpResponse()->redirect('news-' . $request->getData('news') . '.php');
            } else {
                $this->page->addVar('erreurs', $comment->erreurs());
            }
            $this->page->addVar('comment', $comment);
        }
    }

    public function executeIndex(HTTPRequest $request)
    {
        $nombreNews = $this->app->config()->get('nombre_news');
        $nombreCaracteres = $this->app->config()->get('nombres_caracteres');

        //definition du titre
        $this->page->addVar('title', 'Liste des ' . $nombreNews . 'dernières news');

        //manager news
        $manager = $this->managers->getManagerOf('News');

        $listeNews = $manager->getList(0, $nombreNews);
        foreach ($listeNews as $news) {
            if (strlen($news->contenu()) > $nombreCaracteres) {
                $debut = substr($news->contenu(), 0, $nombreCaracteres);
                $debut = substr($debut, 0, strrpos($debut, ' ')) . '...';

                $news->setContenu($debut);
            }
        }

        $this->page->addVar('listeNews', $listeNews);
    }

    public function executeShow(HTTPRequest $request)
    {
        $news = $this->managers->getManagerOf('News')->getUnique($request->getData('id'));

        if (empty($news)) {
            $this->app->httpResponse()->redirect404();
        }

        $this->page->addVar('title', $news->titre());
        $this->page->addVar('news', $news);
        $this->page->addVar('comments', $this->managers->getManagerOf('Comments')->getListOf($news->id()));
    }
}