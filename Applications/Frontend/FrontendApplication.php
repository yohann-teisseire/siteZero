<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 23/07/15
 * Time: 10:51
 */

namespace Applications\Frontend;


use Library\Application;

class FrontendApplication extends Application{


    function __construct()
    {
        parent::__construct();

        $this->name = 'Frontend';
    }

    public function run()
    {
        $controller = $this->getController();
        $controller->execute();

        $this->httpResponse->setPage($controller->page());
        $this->httpResponse->send();
    }
}