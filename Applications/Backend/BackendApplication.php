<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 24/07/15
 * Time: 10:29
 */

namespace Applications\Backend;


use Applications\Backend\Modules\Connexion\ConnexionController;
use Library\Application;

class BackendApplication extends Application{

    public function __construct(){
        parent::__construct();
        $this->name = 'Backend';
    }

    public function run(){
        if($this->user->isAuthenticated()){
            $controller = $this->getController();
        }else{
            $controller = new ConnexionController($this,'Connexion', 'index');
        }

        $controller->execute();

        $this->httpResponse->setPage($controller->page());
        $this->httpResponse->send();
    }
}