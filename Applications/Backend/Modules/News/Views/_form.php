<form action="" method="post">
    <p>
        <?php if (isset($erreurs) && in_array(\Library\Entities\News::AUTEUR_INVALIDE, $erreurs)) echo 'Auteur invalide'; ?>
        <label>Auteur</label>
        <input type="text" name="auteur" value="<?php if (isset($news)) echo $news['auteur']; ?>">

        <?php if (isset($erreurs) && in_array(\Library\Entities\News::TITRE_INVALIDE, $erreurs)) echo 'Titre invalude'; ?>
        <label>Titre</label>
        <input type="text" name="tite" value="<?php if (isset($news)) echo $news['titre']; ?>">

        <?php if (isset($erreurs) && in_array(\Library\Entities\News::CONTENU_INVALIDE, $erreurs)) echo 'Contenu invalide'; ?>
        <label>Contenu</label>
        <textarea name="contenu"><?php if (isset($news)) echo $news['contenu']; ?></textarea>


        <?php if (isset($news) && !$news->isNew()) { ?>
            <input type="hidden" name="id" value="<?php echo $news['id']; ?>">
            <input type="submit" value="Modifier" name="modifier">
        <?php
        } else { ?>
            <input type="submit" value="Ajouter" name="ajouter">
        <?php
        }
        ?>
    </p>
</form>