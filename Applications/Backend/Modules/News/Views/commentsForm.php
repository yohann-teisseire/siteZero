<form action="" method="post">
    <p>
        <?php if (isset($erreurs) && in_array(\Library\Entities\Comments::AUTEUR_INVALIDE, $erreurs)) echo 'Auteur invalide'; ?>
        <label>Pseudo</label>
        <input type="text" name="pseudo" value="<?php if (isset($comment)) echo htmlspecialchars($comment['auteur']); ?>">


        <?php if (isset($erreurs) && in_array(\Library\Entities\Comments::CONTENU_INVALIDE, $erreurs)) echo 'Contenu invalide'; ?>
        <label>Contenu</label>
        <textarea name="contenu"><?php if (isset($comment)) echo htmlspecialchars($comment['contenu']); ?></textarea>

        <input type="hidden" value="<?php echo $comment['news']; ?>" name="news">

        <input type="submit" value="Modifier">

    </p>
</form>