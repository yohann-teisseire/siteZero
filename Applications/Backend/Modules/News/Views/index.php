<div class="table-responsive">
    <table class="table">
        <tr>
            <th>Auteur</th>
            <th>Titre</th>
            <th>Date Ajout</th>
            <th>Date Modif</th>
            <th>Action</th>
        </tr>

        <?php foreach ($listeNews as $news) { ?>
            <tr>
                <td><?php echo $news['auteur']; ?></td>
                <td><?php echo $news['titre']; ?></td>
                <td><?php echo $news['dateAjout']; ?></td>
                <td><?php echo $news['dateModif']; ?></td>
                <td><a href="news-update-<?php echo $news['id']; ?>.php">Modifier</a><a
                        href="news-delete-<?php echo $news['id']; ?>.php">Supprimer</a></td>
            </tr>
        <?php
        }
        ?>
    </table>
</div>