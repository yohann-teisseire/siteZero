<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 24/07/15
 * Time: 10:57
 */

namespace Applications\Backend\Modules\News;


use Library\BackController;
use Library\Entities\Comments;
use Library\Entities\News;
use Library\HTTPRequest;

class NewsController extends BackController
{

    public function executeDeleteComment(HTTPRequest $request){

        $this->managers->getManagerOf('Comments')->delete($request->getData('id'));

        $this->app->user()->setFlash('Le commentaire a bien ete supprimé');

        $this->app->httpResponse()->redirect('.');
    }

    public function executeUpdateComment(HTTPRequest $request){
        $this->page->addVar('title', 'Modification commentaire');

        if($request->postExists('pseudo')){
            $comment = new Comments(array(
                'id' =>$request->postData('id'),
                'auteur' => $request->postData('pseudo'),
                'contenu' => $request->postData('contenu')
            ));

            if($comment->isValid()){
                $this->managers->getManagerOf('Comments')->save($comment);

                $this->app->user()->setFlash('Le commentaire a bien été modifié');

                $this->app->httpResponse()->redirec('/news-'.$request->postData('news').'.php');
            }
            else{
                $this->page->addVar('erreurs', $comment->getErreurs());
            }

            $this->page->addVar('comment', $comment);
        }
        else{
            $this->page->addVar('comment', $this->managers->getManagerOf('Comments')->get($request->getData('id')));
        }
    }

    public function executeDelete(HTTPRequest $request){
        $this->managers->getManagerOf('News')->delete($request->getData('id'));

        $this->app->user()->setFlash('La news a bien ete supprimée !');

        $this->app->httpResponse()->redirect('.');
    }

    public function executeUpdate(HTTPRequest $request)
    {
        if ($request->postExists('auteur')) {
            $this->processForm($request);
        } else {
            $this->page->addVar('news', $this->managers->getManagerOf('News')->getUnique($request->getData('id')));
        }

        $this->page->addVar('title', 'Modification d\’une news');
    }

    public function executeIndex(HTTPRequest $request)
    {
        if ($request->postExists('auteur')) {
            $this->processForm($request);
        }
        $this->page->addVar('title', 'Ajout d\'une news');
    }

    public function processForm(HTTPRequest $request)
    {
        $news = new News(array(
            'auteur' => $request->postData('auteur'),
            'titre' => $request->postData('titre'),
            'contenu' => $request->postData('contenu')
        ));

        if ($request->postExists('id')) {
            $news->setId($request->postData('id'));
        }

        if ($news->isValid()) {
            $this->managers->getManagerOf('News')->save($news);
            $this->app->user()->setFlash($news->isNew() ? 'News bien ajoutée' : 'News bien modifiée');
        } else {
            $this->page->addVar('erreurs', $news->getErreurs());
        }

        $this->page->addVar('news', $news);
    }

}